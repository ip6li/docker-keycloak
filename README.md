# Keycloak

dockerized keycloak, should work with Postgres.

# Config

Hostname in .env is critical. It **must** match called hostname. Examples:

* call by IP address 172.16.0.1 : KC_HOSTNAME=172.16.0.1
* call by hostname kc.example.com : KC_HOSTNAME=kc.example.com

If hostname does not match, you cannot login to admin console.

Please do not consider to use *--hostname-strict=false*, this may yield into security issues.

# PostgreSQL

```
alter database keycloak OWNER TO keycloak;
```

# Test

```
curl --verbose 'https://auth1.ip6.li/realms/matrix/.well-known/openid-configuration'
```

