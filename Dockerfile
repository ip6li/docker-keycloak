FROM quay.io/keycloak/keycloak:latest as builder

ARG KEYCLOAK_ADMIN
ARG KEYCLOAK_ADMIN_PASSWORD

ENV KC_HEALTH_ENABLED=true
ENV KC_METRICS_ENABLED=true

ARG KC_FEATURES

# Configure a database vendor
ENV KC_DB=postgres

WORKDIR /opt/keycloak
# for demonstration purposes only, please make sure to use proper certificates in production instead
RUN keytool -genkeypair -storepass password -storetype PKCS12 -keyalg RSA -keysize 4096 -dname "CN=keycloak.example.com" -alias server -ext "SAN:c=DNS:localhost,DNS:keycloak.example.com,IP:127.0.0.1,IP:172.17.0.1" -keystore conf/server.keystore
#RUN /opt/keycloak/bin/kc.sh build --cache=ispn
RUN /opt/keycloak/bin/kc.sh build ${KC_FEATURES}

FROM quay.io/keycloak/keycloak:latest
COPY --from=builder /opt/keycloak/ /opt/keycloak/

# change these values to point to a running postgres instance
ENV KC_DB=postgres
ARG KC_DB_URL
ARG KC_DB_USERNAME
ARG KC_DB_PASSWORD
ARG KC_HOSTNAME
ENTRYPOINT /opt/keycloak/bin/kc.sh start --db postgres --db-url $KC_DB_URL --proxy-headers xforwarded

